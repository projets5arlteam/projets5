package DAO;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

public class DAO {
	
	  protected Connection conn = null;
	   
	  public DAO(Connection conn){
	    this.conn = conn;
	  }
	  
	//------------------------Utilisateur----------------------------------//
		//vérification en BD si le l'utilisateur existe, création sinon
		public Utilisateur createUtilisateur(Utilisateur obj,Groupe gr) throws Exception{
			Utilisateur utilisateur = obj;
			Groupe groupe = gr;
			ResultSet rr;
			{
			    try
			    {	
			    	PreparedStatement ps = conn.prepareStatement("select idUtilisateur from utilisateur where idUtilisateur=?");
			      try	
			      {
			    	  ps.setString(1,obj.getIdUtilisateur());
			    	  rr = ps.executeQuery();

			          if(rr.next()) {
			        	   utilisateur.setIdUtilisateur(rr.getString(1));
			        	   //TODO 
			          } else { 
			        	  PreparedStatement pss = conn.prepareStatement("insert into utilisateur(idUtilisateur,nom,prenom,mdp,typeUtil) values(?,?,?,?,?)");
			        	  pss.setString(1,utilisateur.getIdUtilisateur());
			        	  pss.setString(2, utilisateur.getNom());
			        	  pss.setString(3, utilisateur.getPrenom());
			        	  pss.setString(4, utilisateur.getMdp());
			        	  pss.setString(5, utilisateur.getTypeUtil());
			        	  pss.executeUpdate();
			          }
			      }
			      finally
			      {
			        ps.close();
			      }
			    }
			    finally
			    {
			      }
			
			}return utilisateur;
		}

		
		public void deleteUtilisateur(Utilisateur obj) throws SQLException {
			Utilisateur utilisateur = obj;
			{
			    try
			    {	
			    	PreparedStatement ps = conn.prepareStatement("delete from utilisateur where idUtilisateur=?");
			      try	
			      {
			    	  ps.setString(1,obj.getIdUtilisateur());
			    	  ps.executeUpdate();

			      } 
			      finally
			      {
			        ps.close();
			      }
			    }
			    finally
			    {
			      }
			
			}
		}

		
		public boolean updateUtilisateur(Utilisateur obj) {
			// TODO Auto-generated method stub
			return false;
		}

		
		public Utilisateur findUtilisateur(String idUtilisateur) throws SQLException {
				   
				Utilisateur utilisateur = new Utilisateur(null, null, null, null, null);
				PreparedStatement ps = null;
				ResultSet rs = null;
			        
			       try
			        {
			          ps = conn.prepareStatement("select * from utilisateur where idUtilisateur=?");
			        
			      try
			      {
			    	  ps.setString(1,idUtilisateur);
			    	  rs = ps.executeQuery();

			          if(rs.next())
			          {
			        	  utilisateur.setIdUtilisateur(rs.getString(1));
			        	  utilisateur.setNom(rs.getString(2));
			        	  utilisateur.setPrenom(rs.getString(3));
			        	  utilisateur.setMdp(rs.getString(4));
			        	  utilisateur.setTypeUtil(rs.getString(5));
			          }
				    }
				    finally
				    {
					  rs.close();
					}
			      }
			      finally
			      {
			        ps.close();
			      }
			    
			    
			       	return utilisateur;
			 }
		
			public Utilisateur findUtilisateur(String idUtilisateur,String mdp) throws SQLException {
				   
				Utilisateur utilisateur = new Utilisateur(null, null, null, null, null);
				PreparedStatement ps = null;
				ResultSet rs = null;
			        
			       try
			        {
			          ps = conn.prepareStatement("select * from utilisateur where idUtilisateur=? and mdp=?");	
			          ps.setString(1, idUtilisateur);
			          ps.setString(2, mdp);
			      try
			      {
			    	  rs = ps.executeQuery();
	
			          if(rs.next())
			          {			        	  
			        	  utilisateur.setIdUtilisateur(rs.getString(1));
			        	  utilisateur.setNom(rs.getString(2));
			        	  utilisateur.setPrenom(rs.getString(3));
			        	  utilisateur.setMdp(rs.getString(4));
			        	  utilisateur.setTypeUtil(rs.getString(5));
			          }
				    }
			      
				    finally
				    {
					  rs.close();
					}
			      }
			      finally
			      {
			        ps.close();
			      }
			    
			    
			       	return utilisateur;
			 }
				
		//-------------------------recuperation des utilisateurs existant ---------------------//
		  
		  public ArrayList<Utilisateur> listerUtilisateurs() throws SQLException 
		  {
			  ArrayList<Utilisateur> utilisateurs = new ArrayList<>();
			  Statement s = null;
			  ResultSet rss = null;
			  
			  try
		      {
			  	s = conn.createStatement();
			  	try 
			  	{
			  		String req = "select idUtilisateur,nom,prenom,mdp,typeUtil from utilisateur";
				  	rss = s.executeQuery(req);
				  	
				  	while(rss.next()) {
			        	  Utilisateur u = new Utilisateur(null,null,null,null,null);
			        	  u.setIdUtilisateur(rss.getString(1));
			        	  u.setNom(rss.getString(2));
			        	  u.setPrenom(rss.getString(3));
			        	  u.setMdp(rss.getString(4));
			        	  u.setTypeUtil(rss.getString(5));
			        	  utilisateurs.add(u);
			          }
				  	
			  	}
			  	finally
			    {
				  rss.close();
				}
			  }
			finally
			{
			  s.close();
			}
			  
			  return utilisateurs;
		  }
		  
		  public HashMap<String,Utilisateur> listerMapUtilisateurs() throws SQLException 
		  {
			  HashMap<String,Utilisateur> utilisateurs = new HashMap<>();
			  Statement s = null;
			  ResultSet rss = null;
			  
			  try
		      {
			  	s = conn.createStatement();
			  	try 
			  	{
			  		String req = "select idUtilisateur,nom,prenom,mdp,typeUtil from utilisateur";
				  	rss = s.executeQuery(req);
				  	
				  	while(rss.next()) {
			        	  Utilisateur u = new Utilisateur(null,null,null,null,null);
			        	  u.setIdUtilisateur(rss.getString(1));
			        	  u.setNom(rss.getString(2));
			        	  u.setPrenom(rss.getString(3));
			        	  u.setMdp(rss.getString(4));
			        	  u.setTypeUtil(rss.getString(5));
			        	  utilisateurs.put(u.getIdUtilisateur(),u);
			          }
				  	
			  	}
			  	finally
			    {
				  rss.close();
				}
			  }
			finally
			{
			  s.close();
			}
			  
			  return utilisateurs;
		  }
		  
		  public boolean checkUtilisateur(Utilisateur util, String mdp) {
			  boolean reponse = false;
			  
			  
			  
			  return reponse;
		  }

		
		/*****************************GROUPE*****************************************************/
		//vérification en BD si le l'utilisateur existe, création sinon
				public Groupe createGroupe(Groupe obj) throws Exception{
					Groupe groupe = obj;
					ResultSet rr;
					{
					    try
					    {	
					    	PreparedStatement ps = conn.prepareStatement("select * from groupe where libelle=?");
					      try	
					      {
					    	  ps.setString(1,obj.getLibelle());
					    	  rr = ps.executeQuery();

					          if(rr.next()) {
					        	   groupe.setIdGroupe(rr.getInt(1));
					        	   groupe.setLibelle(rr.getString(2));
					          } else { 
					        	  PreparedStatement pss = conn.prepareStatement("insert into groupe(libelle) values(?)");
					        	  pss.setString(1,groupe.getLibelle());
					        	  pss.executeUpdate();
					          }
					      }
					      finally
					      {
					        ps.close();
					      }
					    }
					    finally
					    {
					      }
					
					}return groupe;
				}

				
				public void deleteGroupe(Groupe obj) throws SQLException {
					Groupe groupe = obj;
					{
					    try
					    {	
					    	PreparedStatement ps = conn.prepareStatement("delete from groupe where libelle=?");
					      try	
					      {
					    	  ps.setString(1,obj.getLibelle());
					    	  ps.executeUpdate();

					      } 
					      finally
					      {
					        ps.close();
					      }
					    }
					    finally
					    {
					      }
					
					}
				}

				
				public boolean updateGroupe(Groupe obj) {
					// TODO Auto-generated method stub
					return false;
				}

				
				public Groupe findGroupe(String libelle) throws SQLException {
						   
						Groupe groupe = new Groupe(0,null);
						PreparedStatement ps = null;
						ResultSet rs = null;
					        
					       try
					        {
					          ps = conn.prepareStatement("select * from groupe where libelle=?");
					        
					      try
					      {
					    	  ps.setString(1,libelle);
					    	  rs = ps.executeQuery();

					          if(rs.next())
					          {
					        	  groupe.setIdGroupe(rs.getInt(1));
					        	  groupe.setLibelle(rs.getString(2));
					          }
						    }
						    finally
						    {
							  rs.close();
							}
					      }
					      finally
					      {
					        ps.close();
					      }
					    
					    
					       	return groupe;
					 }
				
				public Groupe findGroupe(int idGroupe) throws SQLException {
					   
					Groupe groupe = new Groupe(0,null);
					PreparedStatement ps = null;
					ResultSet rs = null;
				        
				       try
				        {
				          ps = conn.prepareStatement("select * from groupe where idGroupe=?");
				        
				      try
				      {
				    	  ps.setInt(1,idGroupe);
				    	  rs = ps.executeQuery();

				          if(rs.next())
				          {
				        	  groupe.setIdGroupe(rs.getInt(1));
				        	  groupe.setLibelle(rs.getString(2));
				          }
					    }
					    finally
					    {
						  rs.close();
						}
				      }
				      finally
				      {
				        ps.close();
				      }
				    
				    
				       	return groupe;
				 }
				
			//-------------------------recuperation des groupes existant ---------------------//
			  
			  public ArrayList<Groupe> listerGroupes() throws SQLException 
			  {
				  int nbC = 0;
				  ArrayList<Groupe> groupes = new ArrayList<>();
				  Statement s;
				  ResultSet rs = null;
				  ResultSet rss = null;
				  try
			      {
				  	s = conn.createStatement();
				  	try 
				  	{
					  	rs = s.executeQuery("select count(*) from groupe");
					  	
					  	if(rs.next()) { 
					  		nbC = rs.getInt(1);
					  	}
				        
					  	
					  	int nb = 1;
					  	while ( groupes.size() != nbC ) {
					  		
						  	String req = "select * from groupe where idGroupe="+nb;
						  	rss = s.executeQuery(req);
						  	
					          if(rss.next())
					          {
					        	  
					        	  Groupe g = new Groupe(0, "");
					        	  g.setIdGroupe(rss.getInt(1));
					        	  g.setLibelle(rss.getString(2));			        
					        	  groupes.add(g);
					        	  
					          }
					          nb++;
					  	}
				  	}
					  	
				  	finally
				    {
					  rss.close();
					}
			      }
			      finally
			      {
			        rs.close();
			      }
			  
				  return groupes;
			  }
				
		/*****************************Message*****************************************************/
				public Message createMessage(Message obj) throws Exception{
					Message message = obj;
					PreparedStatement ps = null;
					PreparedStatement pss = null;
					ResultSet rss = null;
					{
					    try
					    {	
				        	  ps = conn.prepareStatement("insert into message(dateM,message,idFil,idUtilisateur) values(?,?,?,?)");
				        	  ps.setString(1,message.getDate());
				        	  ps.setString(2,message.getMessage());
				        	  ps.setInt(3, message.getIdFil());
				        	  ps.setString(4, message.getIdUtilisateur());
				        	  ps.executeUpdate();
				        	  
				        	//récuperation de l'objet
				        	  pss = conn.prepareStatement("select idMessage from message where dateM=? and message=? and idFil=? and idUtilisateur=?");
				        	  pss.setString(1,message.getDate());
				        	  pss.setString(2,message.getMessage());
				        	  pss.setInt(3, message.getIdFil());
				        	  pss.setString(4, message.getIdUtilisateur());
				        	  rss = pss.executeQuery();
				        	  
				        	  while(rss.next()){
				        		  message.setIdMessage(rss.getInt(1));
				        	  }
					          
					     }
					      finally
					      {
					        ps.close();
					      }
					    
					    
					
					}return message;
				}
				
				//lister les message d'un fil de discussion
				public void listerMessagePourFilDeDiscussion(FilDeDiscussion obj) throws Exception{
					FilDeDiscussion fil = obj;
					PreparedStatement pss = null;
					ResultSet rss = null;
					{
					    try
					    {	
				        	  
				        	//récuperation de l'objet
				        	  pss = conn.prepareStatement("select * from message where idFil=?");
				        	  pss.setInt(1, fil.getIdFil());  
				        	  rss = pss.executeQuery();
				        	  
				        	  while(rss.next()){
				        		  Message m = new Message(0,null,null,0,null);
				        		  m.setIdMessage(rss.getInt(1));
				        		  m.setDate(rss.getString(2));
				        		  m.setMessage(rss.getString(3));
				        		  m.setIdFil(rss.getInt(4));
				        		  m.setIdUtilisateur(rss.getString(5));
				        		  fil.ajouterMessage(m);
				        	  }
					          
					     }
					      finally
					      {
					        pss.close();
					      }
					    
					    
					
					}
				}
			
				
		/*****************************FilDeDiscussion*****************************************************/
				public FilDeDiscussion createFilDeDiscussion(FilDeDiscussion obj) throws Exception{
					FilDeDiscussion fil = obj;
					PreparedStatement ps = null;
					PreparedStatement pss = null;
					ResultSet rss = null;
					{
					    try
					    {	
				        	  ps = conn.prepareStatement("insert into fildediscussion(titre,premierMessage,idGroupe,idUtilisateur) values(?,?,?,?)");
				        	  ps.setString(1,fil.getTitre());
				        	  ps.setString(2,fil.getPremierMessage());
				        	  ps.setInt(3, fil.getIdGroupe());
				        	  ps.setString(4, fil.getIdUtilisateur());
				        	  ps.executeUpdate();
				        	  
				        	  //récuperation de l'objet
				        	  pss = conn.prepareStatement("select idFil from fildediscussion where titre=? and premierMessage=? and idGroupe=? and idUtilisateur=?");
				        	  pss.setString(1,fil.getTitre());
				        	  pss.setString(2,fil.getPremierMessage());
				        	  pss.setInt(3, fil.getIdGroupe());
				        	  pss.setString(4, fil.getIdUtilisateur());
				        	  rss = pss.executeQuery();
				        	  
				        	  while(rss.next()){
				        		  fil.setIdFil(rss.getInt(1));
				        	  }
				        	  
				        	  //creation du premier message correspondant
				        	  DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
				      		  Date date = new Date();
				      		  String dateM = dateFormat.format(date);
				      		  Message m = new Message(0,dateM,fil.getPremierMessage(),fil.getIdFil(),fil.getIdUtilisateur());
				      		  m = this.createMessage(m);
				      		  fil.ajouterMessage(m);
				        	  
					     }
					      finally
					      {
					        ps.close();
					      }
					    
					    
					
					}return fil;
				}
				
				//-------------------------lister les fils de dicussions pour un utilisateur--//
				 public Set<FilDeDiscussion> listerFilsDeDiscussions(Utilisateur util) throws SQLException 
				  {
					  Set<FilDeDiscussion> fils = new HashSet<>();
					  PreparedStatement ps = null;
					  ResultSet rss = null;
					  
					  try
				      {
					  	ps = conn.prepareStatement("select * from fildediscussion where idUtilisateur=?");
					  	ps.setString(1, util.getIdUtilisateur());
					  	try 
					  	{
					  		
						  	rss = ps.executeQuery();
						  	
						  	while(rss.next()) {
					        	  FilDeDiscussion f = new FilDeDiscussion(0,null,null,0,null);
					        	  f.setIdFil(rss.getInt(1));
					        	  f.setTitre(rss.getString(2));
					        	  f.setPremierMessage(rss.getString(3));
					        	  f.setIdGroupe(rss.getInt(4));
					        	  f.setIdUtilisateur(rss.getString(5));
					        	  fils.add(f);
					          }
						  	
					  	}
					  	finally
					    {
						  rss.close();
						}
					  }
					finally
					{
					  ps.close();
					}
					  
					  return fils;
				  }
				 
				//-------------------------lister les fils de dicussions pour chaque groupes faisant partie + cree--//
				 public void listerFilsDeDiscussions(Groupe gr, Set<FilDeDiscussion> filsDeDiscussions) throws SQLException 
				  {
					  //Set<FilDeDiscussion> fils = filsDeDiscussions;
					  PreparedStatement ps = null;
					  ResultSet rss = null;
					  
					  try
				      {
					  	ps = conn.prepareStatement("select * from fildediscussion where idGroupe=?");
					  	ps.setInt(1, gr.getIdGroupe());
					  	try 
					  	{
					  		
						  	rss = ps.executeQuery();
						  	
						  	while(rss.next()) {
					        	  FilDeDiscussion f = new FilDeDiscussion(0,null,null,0,null);
					        	  f.setIdFil(rss.getInt(1));
					        	  f.setTitre(rss.getString(2));
					        	  f.setPremierMessage(rss.getString(3));
					        	  f.setIdGroupe(rss.getInt(4));
					        	  f.setIdUtilisateur(rss.getString(5));
					        	  filsDeDiscussions.add(f);
					          }
						  	
					  	}
					  	finally
					    {
						  rss.close();
						}
					  }
					finally
					{
					  ps.close();
					}
					  					  
				  }
				
				
				//-------------------------recuperation des messages pour un fil de discussion ---------------------//
				  
				  public void listerMessages(FilDeDiscussion fil) throws SQLException 
				  {
					  ArrayList<Message> messages = new ArrayList<>();
					  PreparedStatement ps = null;
					  ResultSet rss = null;
					  
					  try
				      {
					  	ps = conn.prepareStatement("select * from message where idFil=?");
					  	try 
					  	{
					  		ps.setInt(1,fil.getIdFil());
						  	rss = ps.executeQuery();
						  	
						  	while(rss.next()) {
					        	  Message m = new Message(0,null,null,0,null);
					        	  m.setIdMessage(rss.getInt(1));
					        	  m.setDate(rss.getString(2));
					        	  m.setMessage(rss.getString(3));
					        	  m.setIdFil(rss.getInt(4));
					        	  m.setIdUtilisateur(rss.getString(5));
					        	  messages.add(m);
					          }
						  	fil.setMessages(messages);
						  	
					  	}
					  	finally
					    {
						  rss.close();
						}
					  }
					finally
					{
					  ps.close();
					}
				  }
				
	  /****************************************appartenir******************************************/
				public void createAppartenir(Utilisateur obj, Groupe obj2) throws Exception{
					Utilisateur util = obj;
					Groupe gr = obj2;
					System.out.println(util.getIdUtilisateur() + " " + gr.getIdGroupe());
					PreparedStatement ps = null;
					{
					    try
					    {	
				        	  ps = conn.prepareStatement("insert into appartenir(idUtilisateur,idGroupe) values(?,?)");
				        	  ps.setString(1,util.getIdUtilisateur());
				        	  ps.setInt(2,gr.getIdGroupe());
						      ps.executeUpdate();   
					     }
					      finally
					      {
					        ps.close();
					      }
					    
					    
					
					}
				}
				
				//lister les groupes auquels un utilisateur appartient
				public ArrayList<Groupe> listerGroupesUtilisateur(Utilisateur obj) throws Exception{
					Utilisateur util = obj;
					ArrayList<Groupe> groupes = new ArrayList<Groupe>();
					PreparedStatement ps = null;
					ResultSet rss = null;
					{
					    try
					    {	
				        	  ps = conn.prepareStatement("select idGroupe from appartenir where idUtilisateur=?");
				        	  ps.setString(1,util.getIdUtilisateur());;
				        	  rss = ps.executeQuery();
				        	  
				        	  while(rss.next()) {
				        		  Groupe g = new Groupe(0,null);
					        	  g = this.findGroupe(rss.getInt(1));
					        	  groupes.add(g);				        		  
				        	  }					          
					     }
					      finally
					      {
					        ps.close();
					      }					    
					    			
					}return groupes;
				}
				
				public HashMap<String,Groupe> listerMapGroupes() throws SQLException 
				  {
					  HashMap<String,Groupe> groupes = new HashMap<>();
					  Statement s = null;
					  ResultSet rss = null;
					  
					  try
				      {
					  	s = conn.createStatement();
					  	try 
					  	{
					  		String req = "select * from groupe";
						  	rss = s.executeQuery(req);
						  	
						  	while(rss.next()) {
						  		Groupe g = new Groupe(0, "");
					        	  g.setIdGroupe(rss.getInt(1));
					        	  g.setLibelle(rss.getString(2));			        
					        	  groupes.put(g.getLibelle(),g);
					          }
						  	
					  	}
					  	finally
					    {
						  rss.close();
						}
					  }
					finally
					{
					  s.close();
					}
					  
					  return groupes;
				  }
}