package DAO;

import java.io.Serializable;

public class Message implements Serializable{
	
	private int idMessage;
	private String date;
	private String message;
	
	private int idFil;
	private String idUtilisateur;
	
	public Message(int idMessage, String date, String message, int idFil, String idUtilisateur) {

		this.idMessage = idMessage;
		this.date = date;
		this.message = message;
		this.idFil = idFil;
		this.idUtilisateur = idUtilisateur;
	}

	public int getIdMessage() {
		return this.idMessage;
	}

	public void setIdMessage(int idMessage) {
		this.idMessage = idMessage;
	}

	public String getDate() {
		return this.date;
	}

	public void setDate(String date) {
		this.date = date;
	}

	public String getMessage() {
		return this.message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public int getIdFil() {
		return this.idFil;
	}

	public void setIdFil(int idFil) {
		this.idFil = idFil;
	}

	public String getIdUtilisateur() {
		return this.idUtilisateur;
	}

	public void setIdUtilisateur(String idUtilisateur) {
		this.idUtilisateur = idUtilisateur;
	}

	@Override
	public String toString() {
		return "Message [idMessage=" + idMessage + ", date=" + date + ", message=" + message + ", idFil=" + idFil
				+ ", idUtilisateur=" + idUtilisateur + "]";
	}
	
	
	
	

}
