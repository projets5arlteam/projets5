package DAO;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class FilDeDiscussion implements Serializable{
	
	private int idFil;
	private String titre;
	private String premierMessage;
	
	private int idGroupe;
	private String idUtilisateur;
	
	private List<Message> messages;
	
	public FilDeDiscussion(int idFil, String titre, String premierMessage,int idGroupe, String idUtilisateur) {

		this.idFil = idFil;
		this.titre = titre;
		this.premierMessage = premierMessage;
		this.idGroupe = idGroupe;
		this.idUtilisateur = idUtilisateur;
		this.messages = new ArrayList<Message>();
		
	}

	
	public List<Message> getMessages() {
		return messages;
	}

	public void setMessages(List<Message> messages) {
		this.messages = messages;
	}
	
	public void ajouterMessage(Message message) {
		this.messages.add(message);
	}

	public int getIdFil() {
		return this.idFil;
	}

	public void setIdFil(int idFil) {
		this.idFil = idFil;
	}

	public String getTitre() {
		return this.titre;
	}

	public void setTitre(String titre) {
		this.titre = titre;
	}

	public String getPremierMessage() {
		return this.premierMessage;
	}

	public void setPremierMessage(String premierMessage) {
		this.premierMessage = premierMessage;
	}
	
	public int getIdGroupe() {
		return this.idGroupe;
	}

	public void setIdGroupe(int idGroupe) {
		this.idGroupe = idGroupe;
	}

	public String getIdUtilisateur() {
		return this.idUtilisateur;
	}

	public void setIdUtilisateur(String idUtilisateur) {
		this.idUtilisateur = idUtilisateur;
	}

	@Override
	public String toString() {
		return "FilDeDiscussion [idFil=" + idFil + ", titre=" + titre + ", premierMessage=" + premierMessage
				+ ", idGroupe=" + idGroupe + ", idUtilisateur=" + idUtilisateur + ", messages=" + messages + "]";
	}


	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + idFil;
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FilDeDiscussion other = (FilDeDiscussion) obj;
		if (idFil != other.idFil)
			return false;
		return true;
	}
	
	
	

	
}
