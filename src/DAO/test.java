package DAO;


import java.awt.Frame;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.print.attribute.standard.DateTimeAtCreation;
import javax.swing.JFrame;

import Client.Mod�leClient;
import Serveur.VueServeur;

public class test {

	public static void main(String[] args) throws Exception {
		
		//lien pour les JTablela 
		//https://openclassrooms.com/forum/sujet/java-afficher-des-resultats-sql-sous-forme-de-jtable-72995

		//test connexion / deco 
		ConnexionBD.getInstance();		
		//ConnexionBD.deconnexion();
		
		/*******************************************************************/
		/***********************test classe utilisateur*********************/
		DAO bdd = new DAO(ConnexionBD.getInstance());
		Utilisateur u = new Utilisateur("Niska", "Abdou", "Dylan","bendo", "�l�ve");
		//bdd.createUtilisateur(u);
		
		Utilisateur u2 = bdd.findUtilisateur("Niska");
		//System.out.println(u2.getPrenom());
		
		/***********************test classe groupe*********************/
		Groupe g = new Groupe(0,"L3 TD1");
		//bdd.createGroupe(g);
		
		Groupe g2 = bdd.findGroupe("L3 TD");
		//System.out.println(g2.getLibelle() + " " + g2.getIdGroupe());
		
		/***********************test classe message*********************/
		DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
		Date date = new Date();
		String dateM = dateFormat.format(date);
		//marchera quand un fil de discusion id 1 existera
		Message m = new Message(0,dateM,"Salut bb",1,"Niska");
		//System.out.println(dateM);
		//m = bdd.createMessage(m);
		//System.out.println(m);

		/***********************test classe fil*********************/
		FilDeDiscussion f = new FilDeDiscussion(1, "Gros le chauffage", "�a marche pas", 1,"Niska");
		//f = bdd.createFilDeDiscussion(f);
		//bdd.listerMessages(f);
		
		//System.out.println(f.getMessages());
		
		//test appartenir et lister groupe + util
		//System.out.println(bdd.listerGroupesUtilisateur(u));
		
		
		//pour gerer la recup�ration des id des message, fil, groupe, on fera une mise a jour de nos map, 
		//en gros recuperer tout le monde apr�s chaque insertion 
		//( peut s'averer couteux), afin de mettre a jour la map
		//ou alors recuperer �a lors des insertion via un ptit select
		
		
		//test chargement fils cree par un utilisateur
		Set<FilDeDiscussion> fils = new HashSet<>();
		//fils = bdd.listerFilsDeDiscussions(u);
		//System.out.println(fils);
		
		//Mod�leClient mc = new Mod�leClient(u);
		//mc.chargerFilsDeDicussions();
		//mc.chargerGroupesActifs();
		
			
		
		//test VueServeur
		Frame frame = new JFrame();
		VueServeur v = new VueServeur();
		frame.add(v);
		frame.setSize(800, 500);
		//D�finit un titre pour notre fen�tre
		frame.setTitle("VueServeur");
		//Termine le processus lorsqu'on clique sur la croix rouge
		((JFrame) frame).setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		//v.ecireLog("Salut");
		//v.ecireLog("Bendo");
		
	}

}
