package DAO;

import java.io.Serializable;

public class Groupe implements Comparable<Groupe>,Serializable{
	
	private int idGroupe;
	private String libelle;
	
	public Groupe(int idGroupe, String libelle) {
		
		this.idGroupe = idGroupe;
		this.libelle = libelle;
	}

	public int getIdGroupe() {
		return this.idGroupe;
	}

	public void setIdGroupe(int idGroupe) {
		this.idGroupe = idGroupe;
	}

	public String getLibelle() {
		return this.libelle;
	}

	public void setLibelle(String libelle) {
		this.libelle = libelle;
	}

	@Override
	public String toString() {
		return "Groupe [idGroupe=" + idGroupe + ", libelle=" + libelle + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((libelle == null) ? 0 : libelle.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Groupe other = (Groupe) obj;
		if (libelle == null) {
			if (other.libelle != null)
				return false;
		} else if (!libelle.equals(other.libelle))
			return false;
		return true;
	}

	@Override
	public int compareTo(Groupe o) {
		return this.libelle.compareTo(o.getLibelle());
	}
	
	
	

}
