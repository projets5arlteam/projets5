package DAO;

import java.io.Serializable;

public class Utilisateur implements Serializable{
	
	private String IdUtilisateur;
	private String nom;
	private String prenom;
	private String mdp;
	private String typeUtil;
	
	
	public Utilisateur(String idUtilisateur, String nom, String prenom, String mdp, String typeUtil) {

		this.IdUtilisateur = idUtilisateur;
		this.nom = nom;
		this.prenom = prenom;
		this.mdp = mdp;
		this.typeUtil = typeUtil;
	}

	public String getIdUtilisateur() {
		return this.IdUtilisateur;
	}

	public void setIdUtilisateur(String idUtilisateur) {
		this.IdUtilisateur = idUtilisateur;
	}

	public String getNom() {
		return this.nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrenom() {
		return this.prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getMdp() {
		return this.mdp;
	}

	public void setMdp(String mdp) {
		this.mdp = mdp;
	}

	public String getTypeUtil() {
		return this.typeUtil;
	}

	public void setTypeUtil(String typeUtil) {
		this.typeUtil = typeUtil;
	}
	
	public boolean checkNull() {
		return (this.getIdUtilisateur().length() != 0 && this.getNom().length() != 0 && this.getPrenom().length() != 0 && this.getMdp().length() != 0 && this.getTypeUtil().length() != 0);
	}
	
	
	

}
