package DAO;
import java.sql.*;

public class ConnexionBD {

		  private static Connection conn;
	   
		  public static Connection getInstance() throws ClassNotFoundException, InstantiationException, IllegalAccessException {

		    if(conn == null){
		      try {
		    	 // connexion � la base de donn�e par : jdbc:mysql://nomhote:port/numbed
		    	 Class.forName("com.mysql.jdbc.Driver");
		    	 
		    	 String url = "jdbc:mysql://localhost/projets5?useSSL=false";
		    	 String user = "dylan";
		    	 String passwd = "limon";
		    	 
		    	 //System.out.println("Driver O.K.");
		    	 
		    	 conn = DriverManager.getConnection(url, user, passwd);

					            
		      } catch (SQLException e) {
		    	  e.printStackTrace();
		      }
		    }      
		    return conn;
		  }   

		

		  public static void deconnexion() throws Exception {
			  
			  if(conn != null)
				  try {
					  conn.close();	
			      } catch ( SQLException ignore ) {
			           //ignore
			      }				 		            
		  }	
		  
}
