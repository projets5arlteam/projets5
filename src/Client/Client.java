package Client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import DAO.FilDeDiscussion;
import DAO.Message;
import DAO.Utilisateur;

public class Client {

	Socket socket;
	ObjectInputStream oIn;
	ObjectOutputStream oOut;
	
	public Client() {

	int port = 8081;
	
	try {
		String serverHostname = new String("127.0.0.1");

		//System.out.println("Connecting to host " + serverHostname + " on port " + port + ".");

		socket = null;
		
		oIn = null;
		oOut = null;
		try {
			socket = new Socket(serverHostname, 8081);
			oOut = new ObjectOutputStream(socket.getOutputStream());
			oIn = new ObjectInputStream(socket.getInputStream());
		} catch (UnknownHostException e) {
			System.err.println("Unknown host: " + serverHostname);
			System.exit(1);
		} catch (IOException e) {
			System.err.println("Unable to get streams from server");
			System.exit(1);
		}

	} catch (Exception e) {
		e.printStackTrace();
	}
	
	}


	public Utilisateur connexion(Utilisateur util) {
		Utilisateur reponse = new Utilisateur(null,null,null,null,null);
		Utilisateur utilTest = new Utilisateur(null,null,null,null,null);
		utilTest.setIdUtilisateur(util.getIdUtilisateur());
		utilTest.setMdp(util.getMdp());
		try {
			oOut.writeObject(utilTest);
			oOut.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			reponse = (Utilisateur) oIn.readObject();
		} catch (ClassNotFoundException | IOException e) {
			e.printStackTrace();
		}
		
		return reponse;
	}

	public Donnees recevoirDonnes() {
		Donnees donnees = new Donnees();
		try {
			donnees = (Donnees) oIn.readObject(); 
		} catch (IOException e) {
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		
		return donnees;
	}


	public void deconnexion() {
		String deconnexion = new String("deconnexion");
		try {
			oOut.writeObject(deconnexion);
			oOut.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}


	public Message ecrireMessage(Message m) {
		String message = new String("message");
		try {
			oOut.writeObject(message);
			oOut.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			oOut.writeObject(m);
			oOut.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		try {
			m = (Message) oIn.readObject();
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	return m;
	}


	public void ecrireFilDeDiscussion(FilDeDiscussion fil) {
		String message = new String("filDeDiscussion");
		try {
			oOut.writeObject(message);
			oOut.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			oOut.writeObject(fil);
			oOut.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

}
