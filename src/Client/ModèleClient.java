package Client;
import DAO.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

public class ModèleClient {

	private Set<FilDeDiscussion> filDeDicussions;
	private ArrayList<Groupe> groupes;	
	private Set<Groupe> groupesActifs;
	private Utilisateur utilisateurCourant;
	private FilDeDiscussion filDeDiscussionCourant;
	private List<Message> messageEnAttente;
	private Map<Integer,Utilisateur> utilisateursCourant;
	private Map<String,Utilisateur> mapUtilisateurs;
	private Map<String,Groupe> mapGroupes;
	private Client client;
	
	public ModèleClient() throws Exception {
		this.filDeDiscussionCourant = new FilDeDiscussion(0, null, null, 0, null);
		this.groupes = new ArrayList<Groupe>();
		this.filDeDicussions = new HashSet<FilDeDiscussion>();
		this.groupesActifs = new TreeSet<Groupe>();
		this.messageEnAttente = new ArrayList<Message>();
		this.utilisateursCourant = new HashMap<Integer,Utilisateur>();
		this.mapUtilisateurs = new HashMap<String,Utilisateur>();
		this.mapGroupes = new HashMap<String,Groupe>();
		this.client = new Client();		
	}
	
	public Client getClient() {
		return client;
	}

	public Set<FilDeDiscussion> getFilDeDicussions() {
		return this.filDeDicussions;
	}

	public Set<Groupe> getGroupesActifs() {
		return this.groupesActifs;
	}
	

	public Utilisateur getUtilisateurCourant() {
		return this.utilisateurCourant;
	}

	public FilDeDiscussion getFilDeDiscussionCourant() {
		return this.filDeDiscussionCourant;
	}

	public void setFilDeDiscussionCourant(FilDeDiscussion filDeDiscussionCourant) {
		this.filDeDiscussionCourant = filDeDiscussionCourant;
	}
		
	
	public Map<String, Utilisateur> getMapUtilisateurs() {
		return mapUtilisateurs;
	}

	public void setMapUtilisateurs(Map<String, Utilisateur> mapUtilisateurs) {
		this.mapUtilisateurs = mapUtilisateurs;
	}

	public void setFilDeDicussions(Set<FilDeDiscussion> filDeDicussions) {
		this.filDeDicussions = filDeDicussions;
	}

	public void setGroupes(ArrayList<Groupe> groupes) {
		this.groupes = groupes;
	}

	public void setGroupesActifs(Set<Groupe> groupesActifs) {
		this.groupesActifs = groupesActifs;
	}

	public void setUtilisateursCourant(Map<Integer, Utilisateur> utilisateursCourant) {
		this.utilisateursCourant = utilisateursCourant;
	}
	

	public Map<String, Groupe> getMapGroupes() {
		return mapGroupes;
	}

	public void setMapGroupes(Map<String, Groupe> mapGroupes) {
		this.mapGroupes = mapGroupes;
	}

	public void ajouterMessageEnAttente(Message m) {
		this.messageEnAttente.add(m);
	}
	
	public void chargerUtilissateursCourant(){
		for(Message m : this.filDeDiscussionCourant.getMessages()) {
			this.utilisateursCourant.put(m.getIdMessage(),this.mapUtilisateurs.get(m.getIdUtilisateur()));
		}
	}

	public Map<Integer, Utilisateur> getUtilisateursCourant() {
		return this.utilisateursCourant;
	}
	
	
	public ArrayList<Groupe> getGroupes() {
		return this.groupes;
	}
	
	public void transfertsDonnes() throws Exception {
		//recuperer les donnees
		Donnees donnees = this.client.recevoirDonnes();
		this.setFilDeDicussions(donnees.getFilDeDicussions());
		this.setGroupes(donnees.getGroupes());
		this.setGroupesActifs(donnees.getGroupesActifs());
		this.setMapUtilisateurs(donnees.getMapUtilisateurs());
		this.setMapGroupes(donnees.getMapGroupes());
	}

	public Utilisateur tentativeDeConnexion(Utilisateur util) {
		return this.client.connexion(util);
	}

	public void setUtilisateurCourant(Utilisateur utilConnexion) {
		this.utilisateurCourant = utilConnexion;
		
	}

	public void deconnexion() {
		this.client.deconnexion();		
	}

	public Message ecrireMessage(Message m) {
		return this.client.ecrireMessage(m);		
	}

	public void ecrireFilDeDiscussion(FilDeDiscussion fil) throws Exception {
		this.client.ecrireFilDeDiscussion(fil);
		this.transfertsDonnes();
		
	}

}
