package Client;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import DAO.FilDeDiscussion;
import DAO.Groupe;
import DAO.Message;
import DAO.Utilisateur;

public class Donnees implements Serializable {

	@Override
	public String toString() {
		return "Donnees [filDeDicussions=" + filDeDicussions + ", groupes=" + groupes + ", groupesActifs="
				+ groupesActifs + ", utilisateurCourant=" + utilisateurCourant + ", messageEnAttente=" + messageEnAttente + ", utilisateursCourant="
				+ utilisateursCourant + "]";
	}

	private Set<FilDeDiscussion> filDeDicussions;
	private ArrayList<Groupe> groupes;
	private Set<Groupe> groupesActifs;
	private Utilisateur utilisateurCourant;
	private List<Message> messageEnAttente;
	private HashMap<Integer,Utilisateur> utilisateursCourant;
	private Map<String,Utilisateur> mapUtilisateurs;
	private Map<String,Groupe> mapGroupes;
	
	public Donnees() {
		mapUtilisateurs = new HashMap<String,Utilisateur>();
	}

	public Set<FilDeDiscussion> getFilDeDicussions() {
		return filDeDicussions;
	}

	public void setFilDeDicussions(Set<FilDeDiscussion> filDeDicussions) {
		this.filDeDicussions = filDeDicussions;
	}

	public ArrayList<Groupe> getGroupes() {
		return groupes;
	}

	public void setGroupes(ArrayList<Groupe> groupes) {
		this.groupes = groupes;
	}

	public Set<Groupe> getGroupesActifs() {
		return groupesActifs;
	}

	public void setGroupesActifs(Set<Groupe> groupesActifs) {
		this.groupesActifs = groupesActifs;
	}

	public Utilisateur getUtilisateurCourant() {
		return utilisateurCourant;
	}

	public void setUtilisateurCourant(Utilisateur utilisateurCourant) {
		this.utilisateurCourant = utilisateurCourant;
	}

	public List<Message> getMessageEnAttente() {
		return messageEnAttente;
	}

	public void setMessageEnAttente(List<Message> messageEnAttente) {
		this.messageEnAttente = messageEnAttente;
	}

	public Map<Integer, Utilisateur> getUtilisateursCourant() {
		return utilisateursCourant;
	}

	public void setUtilisateursCourant(HashMap<Integer, Utilisateur> utilisateursCourant) {
		this.utilisateursCourant = utilisateursCourant;
	}

	public Map<String, Utilisateur> getMapUtilisateurs() {
		return mapUtilisateurs;
	}

	public void setMapUtilisateurs(Map<String, Utilisateur> mapUtilisateurs) {
		this.mapUtilisateurs = mapUtilisateurs;
	}

	public Map<String, Groupe> getMapGroupes() {
		return mapGroupes;
	}

	public void setMapGroupes(Map<String, Groupe> mapGroupes) {
		this.mapGroupes = mapGroupes;
	}
	
	
	
	
}
