package Client;

import java.awt.Dimension;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;

import Serveur.ControleurServeur;
import net.miginfocom.swing.MigLayout;

public class VueClientConnexion extends JFrame {
	
	private JTextField texteIdentifiant;
	private JPasswordField texteMdp;
	
	
	public VueClientConnexion() throws Exception {
		ControleurClient controleur = new ControleurClient(this);
			
		JPanel panel = new JPanel();
		panel.setLayout(new MigLayout("", "[434px,grow]", "[][14px][][][][][][][][][][][][][][][][][][][][][][][][][][][][]"));
		
		JLabel lblAjouter = new JLabel("Université Paul Sabatier III");
		panel.add(lblAjouter, "cell 0 0,alignx center");
		
		JLabel lblNewLabel = new JLabel("Identifiant");
		panel.add(lblNewLabel, "cell 0 5,alignx center");
		
		texteIdentifiant = new JTextField();
		panel.add(texteIdentifiant, "cell 0 6,alignx center");
		texteIdentifiant.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Mot de passe");
		panel.add(lblNewLabel_1, "cell 0 9,alignx center");
		
		texteMdp = new JPasswordField();
		panel.add(texteMdp, "cell 0 10,alignx center");
		texteMdp.setColumns(10);
		
		JButton btnConnexion = new JButton("Connexion");
		panel.add(btnConnexion, "cell 0 13,alignx center");
		btnConnexion.addActionListener(controleur);
		
		this.add(panel);
		
		this.setSize(400, 300);
		this.setMinimumSize(new Dimension(400, 300));		
		this.setVisible(true);
		
	}


	public String getTexteIdentifiant() {
		return this.texteIdentifiant.getText();
	}


	public String getTexteMdp() {
		return this.texteMdp.getText();
	}
	
}
