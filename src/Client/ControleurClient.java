package Client;
import DAO.*;

import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;
import java.util.Set;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import com.sun.org.apache.xerces.internal.util.SynchronizedSymbolTable;

import DAO.ConnexionBD;
import DAO.DAO;
import DAO.FilDeDiscussion;
import DAO.Groupe;
import DAO.Utilisateur;
import Serveur.ControleurServeur.Etat;
import net.miginfocom.swing.MigLayout;

public class ControleurClient implements ActionListener{
	
	public enum Etat {CONNEXION,CLIENT,CREATIONTICKET}
	
	private VueClientConnexion vueConnexion;
	private Etat etatCourant;
	private JButton boutonCourant;
		
	//vue client 
	private Utilisateur utilisateurCourant;
	private VueClient vue;
	private Mod�leClient modele;
	
	//creation d'un ticket
	private JDialog fenetreCreationTicket;
	private JTextField texteTitre;
	private JTextField texteMessage;
	private JComboBox comboBoxGroupes;	
	private JButton validerCreationTicket;
	
	//acceuil
	private Set<Groupe> groupesActifs;
	private Set<FilDeDiscussion> filDeDicussions;
	
	
	
	public ControleurClient(VueClientConnexion vueConnexion) throws Exception {
		this.vueConnexion = vueConnexion;
		this.modele = new Mod�leClient();
		this.etatCourant = Etat.CONNEXION;
	}


	@Override
	public void actionPerformed(ActionEvent e) {
		this.boutonCourant = (JButton) e.getSource();
		switch(this.etatCourant) {
		
		case CONNEXION : 
			
			if( this.boutonCourant.getText() == "Connexion") {
				Utilisateur utilConnexion = new Utilisateur(null,null,null,null,null);	
				if(this.vueConnexion.getTexteIdentifiant().length() != 0 && this.vueConnexion.getTexteMdp().length() != 0) {
					utilConnexion.setIdUtilisateur(this.vueConnexion.getTexteIdentifiant());
					utilConnexion.setMdp(this.vueConnexion.getTexteMdp());
					utilConnexion = this.modele.tentativeDeConnexion(utilConnexion);
					if ( utilConnexion.getNom() != null) {						
						try {
							this.modele.setUtilisateurCourant(utilConnexion);
							this.modele.transfertsDonnes();
						} catch (Exception e1) {
							e1.printStackTrace();
						}
						this.groupesActifs = this.modele.getGroupesActifs();
						this.filDeDicussions = this.modele.getFilDeDicussions();
						
						this.vue = new VueClient(utilConnexion, this);
						this.vueConnexion.dispose();
						this.etatCourant = Etat.CLIENT;					
					} else {
						JOptionPane jop;
						//Bo�te du message d'erreur
						jop = new JOptionPane();
						jop.showMessageDialog(null, "Champs invalides ou incomplets", "Attention", JOptionPane.INFORMATION_MESSAGE);
					}
				}
			}
			break;
			
		case CLIENT : 
			this.utilisateurCourant = this.vue.getUtilisateurCourant();
			//this.boutonCourant = (JButton) e.getSource();
			switch (this.boutonCourant.getText()) {
				case "D�connexion" : 
					this.modele.deconnexion();
					this.vue.dispose();
					break;
				case "Nouveau Ticket" : 
				try {
					this.lancerCreationTicket();
				} catch (SQLException e1) {
					e1.printStackTrace();
				}
					break;
				case "Envoyer" : 
					if(this.vue.getMessageCourant().getText().length() != 0){		
						DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
						Date date = new Date();
						String dateM = dateFormat.format(date);
						Message m = new Message(0,dateM,this.vue.getMessageCourant().getText(),this.modele.getFilDeDiscussionCourant().getIdFil(),this.modele.getUtilisateurCourant().getIdUtilisateur());
						
						m = this.modele.ecrireMessage(m);
						
						this.modele.getFilDeDiscussionCourant().ajouterMessage(m);
						this.vue.getMessageCourant().setText("");
						this.vue.ecrireMessage(m);	
						
					}
					break;
			}
			
			
			break;
		case CREATIONTICKET : 
			this.etatCourant = Etat.CLIENT;
			//this.boutonCourant = (JButton) e.getSource();
			if( this.boutonCourant.getText() == "Valider") {
				if(this.texteTitre.getText().length() != 0 && this.texteMessage.getText().length() != 0) {
					Groupe g = null;
					g = this.modele.getMapGroupes().get(this.comboBoxGroupes.getSelectedItem().toString());
					FilDeDiscussion fil = new FilDeDiscussion(0,this.texteTitre.getText(), this.texteMessage.getText(),g.getIdGroupe(), this.utilisateurCourant.getIdUtilisateur());
					//mise a jour comprise
					try {
						this.modele.ecrireFilDeDiscussion(fil);
					} catch (Exception e2) {
						e2.printStackTrace();
					}
					
					this.groupesActifs = this.modele.getGroupesActifs();
					this.filDeDicussions = this.modele.getFilDeDicussions();
					this.vue.setFilDeDicussions(this.filDeDicussions);
					this.vue.setGroupesActifs(this.groupesActifs);
					this.vue.updateMapFilDeDicussion();
					this.vue.updateMapGroupes();
					this.vue.miseAJourJTree2(fil);
										
					fenetreCreationTicket.dispose();
				}
					
				
			}
			break;
		}
	}
	
	
	public void lancerCreationTicket() throws SQLException {
		this.etatCourant = Etat.CREATIONTICKET;
		
		this.fenetreCreationTicket = new JDialog();
		
		this.fenetreCreationTicket.getContentPane().setLayout(new MigLayout("", "[434px,grow]", "[][14px][][][][][][][][][][][][][][][][][][][][][][][][][][]"));
		{
			JLabel lblAjouter = new JLabel("Cr\u00E9ation d'un nouveau ticket");
			this.fenetreCreationTicket.getContentPane().add(lblAjouter, "cell 0 0,alignx center,aligny top");
		}
		
		JLabel lblTitreDuTicket = new JLabel("Titre du ticket");
		this.fenetreCreationTicket.getContentPane().add(lblTitreDuTicket, "cell 0 3");
		
		texteTitre = new JTextField();
		this.fenetreCreationTicket.getContentPane().add(texteTitre, "cell 0 4,alignx left");
		texteTitre.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Destinataires");
		this.fenetreCreationTicket.getContentPane().add(lblNewLabel, "cell 0 6");
		
		//avec les socket, il sera r�cup�r� du mod�le
		comboBoxGroupes = new JComboBox();
		ArrayList<Groupe> listeGroupes = null;
		listeGroupes = this.modele.getGroupes();
		for (Groupe g : listeGroupes) {
			comboBoxGroupes.addItem(g.getLibelle());
		}
		this.fenetreCreationTicket.getContentPane().add(comboBoxGroupes, "cell 0 7,growx");
		
		JLabel lblerMessage = new JLabel("Message");
		this.fenetreCreationTicket.getContentPane().add(lblerMessage, "cell 0 9");
		
		texteMessage = new JTextField();
		this.fenetreCreationTicket.getContentPane().add(texteMessage, "cell 0 10,growx");
		texteMessage.setColumns(10);
		
		validerCreationTicket = new JButton("Valider");
		this.fenetreCreationTicket.getContentPane().add(validerCreationTicket, "cell 0 13,alignx center");		
		validerCreationTicket.addActionListener(this);
		
		fenetreCreationTicket.getContentPane().add(validerCreationTicket, "cell 0 16,alignx center");
		fenetreCreationTicket.setSize(500, 300);
		fenetreCreationTicket.setMinimumSize(new Dimension(500, 300));
		fenetreCreationTicket.setTitle("Cr�ation d'un ticket");
		fenetreCreationTicket.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		fenetreCreationTicket.setVisible(true);
		
		
	}
	
	public Set<FilDeDiscussion> getFilDeDicussions() {
		return this.filDeDicussions;
	}

	public Set<Groupe> getGroupesActifs() {
		return this.groupesActifs;
	}
	
	public Map<Integer, Utilisateur> getUtilisateursCourant() {
		return this.modele.getUtilisateursCourant();
	}
	
	public void setFilDeDicussionCourant(FilDeDiscussion f){
		this.modele.setFilDeDiscussionCourant(f);
		this.modele.chargerUtilissateursCourant();
	}

	public ArrayList<Groupe> getGroupes() {
		return this.modele.getGroupes();
	}
	
}
