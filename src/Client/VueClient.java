package Client;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTextPane;
import javax.swing.JTree;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreeNode;

import DAO.FilDeDiscussion;
import DAO.Groupe;
import DAO.Message;
import DAO.Utilisateur;
import javafx.scene.layout.Border;

public class VueClient extends JFrame {
	
	private Utilisateur utilisateurCourant;
	private ControleurClient controleur;
	
	JTree tree;
	JTextPane filDeDiscussionCourant;
	JTextField messageCourant;
	JPanel centre;
	JPanel ouest;
	JScrollPane tickets;
	JScrollPane zoneFil;
	JPanel mainPanel;
	
	SimpleAttributeSet style_normal;
	StyledDocument doc;
	
	//acceuil
	private ArrayList<Groupe> groupes;
	private Set<Groupe> groupesActifs;
	private Set<FilDeDiscussion> filDeDicussions;
	private Map<String,FilDeDiscussion> mapFilDeDicussions;
	private Map<Integer,Groupe> mapGroupes;
	private Map<Integer,Utilisateur> utilisateursCourant;
	
	public VueClient (Utilisateur utilsateur, ControleurClient controleur) {
		
		this.setTitle("Partie en Cours");		
		this.setSize(800, 500);		
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);			
		this.setResizable(true);
		
		//instantciation du controleur
		this.controleur = controleur;		
		this.utilisateurCourant = utilsateur;
		this.utilisateursCourant = new HashMap<>();
		
		mainPanel = new JPanel();
		mainPanel.setLayout(new BorderLayout());
		
		//JPanel ouest
		ouest = new JPanel();
		ouest.setLayout(new BorderLayout());
		
			JButton nouveauTicket = new JButton("Nouveau Ticket");
			nouveauTicket.addActionListener(controleur);
			ouest.add(nouveauTicket,BorderLayout.NORTH);
			
			JButton deconnexion = new JButton("D�connexion");
			deconnexion.addActionListener(controleur);
			ouest.add(deconnexion, BorderLayout.SOUTH);
			
			this.filDeDicussions = this.controleur.getFilDeDicussions();
			this.groupesActifs = this.controleur.getGroupesActifs();
			
			//
			this.mapFilDeDicussions = new HashMap<>();
			for(FilDeDiscussion f : this.filDeDicussions) {
				this.mapFilDeDicussions.put(f.getTitre(), f);
			}
			
			this.groupes= new ArrayList<>();
			this.groupes = this.controleur.getGroupes();
			this.updateMapGroupes();
			
			DefaultMutableTreeNode node = new DefaultMutableTreeNode("Tickets");
			for (Groupe g : this.groupesActifs) {
				DefaultMutableTreeNode nodeCourant = new DefaultMutableTreeNode(g.getLibelle());
				for(FilDeDiscussion f : this.filDeDicussions) {
					if(f.getIdGroupe() == g.getIdGroupe()) {
						DefaultMutableTreeNode nodeCourant2 = new DefaultMutableTreeNode(f.getTitre());
						nodeCourant.add(nodeCourant2);
					}
				}
				node.add(nodeCourant);
			}
			
			tree = new JTree(node);
			tree.setRootVisible(true);
			tree.setShowsRootHandles(true);
			tree.addTreeSelectionListener(new TreeSelectionListener() {
			    public void valueChanged(TreeSelectionEvent e) {
			        DefaultMutableTreeNode node = (DefaultMutableTreeNode)
			                           tree.getLastSelectedPathComponent();


			        if (node != null) {
					    Object nodeInfo = node.getUserObject();
					    Set<String> s = mapFilDeDicussions.keySet();
					    if(s.contains(nodeInfo.toString())) {
							controleur.setFilDeDicussionCourant(mapFilDeDicussions.get(nodeInfo.toString()));
							utilisateursCourant = controleur.getUtilisateursCourant();
						    genererFilDeDiscussion(mapFilDeDicussions.get(nodeInfo.toString()));
					    }
			        }
			    }
			});
			JScrollPane tickets = new JScrollPane(tree);
			ouest.add(tickets, BorderLayout.CENTER);
			
		mainPanel.add(ouest,BorderLayout.WEST);
		
		
		//JPanel sud
		JPanel sud = new JPanel();
		sud.setLayout(new BorderLayout());
		
			JPanel texteRep = new JPanel();
			texteRep.setLayout(new GridLayout(1, 3));
			
			JLabel label = new JLabel("");
			texteRep.add(label);
			
			messageCourant = new JTextField();
			//texte.setEditable(false);
			texteRep.add(messageCourant);
			
			JButton envoyer = new JButton("Envoyer");
			envoyer.addActionListener(controleur);
			//envoyer.setEnabled(false);
			texteRep.add(envoyer);
			
			sud.add(texteRep,BorderLayout.CENTER);
		mainPanel.add(sud, BorderLayout.SOUTH);
		
		
		//JPanel centre
		centre = new JPanel();
		centre.setLayout(new BorderLayout());
		
		filDeDiscussionCourant = new JTextPane();
		filDeDiscussionCourant.setEditable(false);
		zoneFil = new JScrollPane(filDeDiscussionCourant);
		centre = new JPanel(new BorderLayout());
		centre.add(zoneFil, BorderLayout.CENTER);
		mainPanel.add(centre, BorderLayout.CENTER);
		
		this.add(mainPanel);
		this.setVisible(true);
		
	}
		

	public Utilisateur getUtilisateurCourant() {
		return this.utilisateurCourant;
	}
	
	
	
	public JTextField getMessageCourant() {
		return this.messageCourant;
	}
	

	public void setGroupesActifs(Set<Groupe> groupesActifs) {
		this.groupesActifs = groupesActifs;
	}



	public void setFilDeDicussions(Set<FilDeDiscussion> filDeDicussions) {
		this.filDeDicussions = filDeDicussions;
	}
	
	public void updateMapFilDeDicussion() {
		this.mapFilDeDicussions = new HashMap<>();
		for(FilDeDiscussion f : this.filDeDicussions) {
			this.mapFilDeDicussions.put(f.getTitre(), f);
		}
	}
	
	public void updateMapGroupes() {
		this.mapGroupes = new HashMap<>();
		for(Groupe g : this.groupesActifs) {
			this.mapGroupes.put(g.getIdGroupe(), g);
		}
		for(Groupe g : this.groupes) {
			this.mapGroupes.put(g.getIdGroupe(), g);
		}
	}


	public void genererFilDeDiscussion(FilDeDiscussion fil){
		/*
		 * Cr�ation du JTextPane
		 */
		filDeDiscussionCourant.setText("");
		
		/*
		 * Cr�ation d'un style
		 */
		this.style_normal = new SimpleAttributeSet();
		StyleConstants.setFontFamily(style_normal, "Calibri");
		StyleConstants.setFontSize(style_normal, 14);

		/*
		 * Cr�ation du style pour l'affichage du titre
		 */
		SimpleAttributeSet style_titre = new SimpleAttributeSet();
		style_titre.addAttributes(style_normal);
		StyleConstants.setForeground(style_titre, Color.BLUE);
		StyleConstants.setUnderline(style_titre, true);
		StyleConstants.setFontSize(style_titre, 18);
		StyleConstants.setBold(style_titre, true);
		
		/*
		 * Cr�ation du style qui permet de centrer le texte
		 */
		SimpleAttributeSet centrer = new SimpleAttributeSet();
		StyleConstants.setAlignment(centrer, StyleConstants.ALIGN_CENTER);
		
		/*
		 * Cr�ation du style qui permet d'afficher les r�f�r�nces
		 */
		SimpleAttributeSet style_citation = new SimpleAttributeSet();
		style_citation.addAttributes(style_normal);
		StyleConstants.setItalic(style_citation, true);
		
		try {

			doc = filDeDiscussionCourant.getStyledDocument();
			
			//Titre du fil de discussion 
			doc.insertString(doc.getLength(), fil.getTitre()+"\n\n", style_titre);
			int fin_titre = doc.getLength();
			
			for( Message m : fil.getMessages()) {
				Utilisateur u =  new Utilisateur(null,null,null,null,null);
				u = this.utilisateursCourant.get(m.getIdMessage());
				doc.insertString(doc.getLength(), u.getNom() + " " + u.getPrenom() + " " + m.getDate()+"\n", style_normal);
				doc.insertString(doc.getLength(), m.getMessage()+"\n\n", style_normal);
			}
			
			
						//doc.insertString(doc.getLength(), auteur+"\n", style_citation);
						//doc.insertString(doc.getLength(), livre, style_citation);
			
			/*
			 * Centrage du titre
			 */
			//Si je le laisse �a centre tout
			//doc.setParagraphAttributes(0, fin_titre, centrer, false);
			
		}
		catch (BadLocationException e) {
			e.printStackTrace();
		}
	}
	
	public void ecrireMessage(Message m){
		try {
			doc.insertString(doc.getLength(), this.utilisateurCourant.getNom() + " " + this.utilisateurCourant.getPrenom() + " " + m.getDate() +"\n", style_normal);
			doc.insertString(doc.getLength(), m.getMessage()+"\n\n", style_normal);
		} catch (BadLocationException e) {
			e.printStackTrace();
		}
		
	}

	
	public void miseAJourJTree2(FilDeDiscussion newFil) {
		boolean trouve = parcourirNoeud(this.tree.getModel().getRoot(), this.mapGroupes.get(newFil.getIdGroupe()).getLibelle(), newFil.getTitre());
		
		if( trouve == false ) {
			DefaultMutableTreeNode nodeCourant = (DefaultMutableTreeNode)this.tree.getModel().getRoot();
			DefaultMutableTreeNode nodeCourant2 = new DefaultMutableTreeNode(this.mapGroupes.get(newFil.getIdGroupe()).getLibelle());
			DefaultMutableTreeNode nodeCourant3 = new DefaultMutableTreeNode(newFil.getTitre());
			nodeCourant2.add(nodeCourant3);
			nodeCourant.add(nodeCourant2);
		}
		
		tree.addTreeSelectionListener(new TreeSelectionListener() {
		    public void valueChanged(TreeSelectionEvent e) {
		        DefaultMutableTreeNode node = (DefaultMutableTreeNode)
		                           tree.getLastSelectedPathComponent();


		        if (node != null) {
				    Object nodeInfo = node.getUserObject();
				    Set<String> s = mapFilDeDicussions.keySet();
				    if(s.contains(nodeInfo.toString())) {
						controleur.setFilDeDicussionCourant(mapFilDeDicussions.get(nodeInfo.toString()));
						utilisateursCourant = controleur.getUtilisateursCourant();
					    genererFilDeDiscussion(mapFilDeDicussions.get(nodeInfo.toString()));
				    }
		        }
		    }
		});
		tree.updateUI();
		tree.validate();
		
	}

	//le rajouter direct dans le JTree
	public boolean parcourirNoeud(Object r,String groupeRecherche,String newfilDeDiscussion) {
		boolean trouve = false; 
		
		DefaultMutableTreeNode root = (DefaultMutableTreeNode) r;
		for (int i = 0; i < root.getChildCount(); i++)
		{
			DefaultMutableTreeNode nodeInfo = (DefaultMutableTreeNode) root.getChildAt(i);
			if (nodeInfo.toString().equals(groupeRecherche) && !trouve) {
				trouve = true;
				DefaultMutableTreeNode nodeCourant2 = new DefaultMutableTreeNode(newfilDeDiscussion);
				nodeInfo.add(nodeCourant2);
			}
			else 
				parcourirNoeud(root.getChildAt(i),groupeRecherche,newfilDeDiscussion);
		}	
		
		return trouve;
	}
	}
