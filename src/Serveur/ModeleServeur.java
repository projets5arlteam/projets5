package Serveur;

import java.net.Socket;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;

import Client.Donnees;
import DAO.ConnexionBD;
import DAO.DAO;
import DAO.FilDeDiscussion;
import DAO.Groupe;
import DAO.Message;
import DAO.Utilisateur;

public class ModeleServeur {
		
		private Serveur serveur;
		private DAO dao;
		private Set<FilDeDiscussion> filDeDicussions;
		private ArrayList<Groupe> groupes;
		private Set<Groupe> groupesActifs;
		private Utilisateur utilisateurCourant;
		private List<Message> messageEnAttente;
		private Map<Integer,Utilisateur> utilisateursCourant;
		private Map<String,Utilisateur> mapUtilisateurs;
		private Map<String,Groupe> mapGroupes;
		
		public ModeleServeur(Socket socket) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
			this.dao = new DAO(ConnexionBD.getInstance());
			this.serveur = new Serveur(socket,this);
			
			this.groupes = new ArrayList<Groupe>();
			this.filDeDicussions = new HashSet<FilDeDiscussion>();
			this.groupesActifs = new TreeSet<Groupe>();
			this.messageEnAttente = new ArrayList<Message>();
			this.utilisateursCourant = new HashMap<Integer,Utilisateur>();
			this.mapUtilisateurs = new HashMap<String,Utilisateur>();
			this.mapGroupes = new HashMap<String,Groupe>();
			
		}
				

		public void setUtilisateurCourant(Utilisateur utilisateurCourant) {
			this.utilisateurCourant = utilisateurCourant;
		}

		public DAO getDao() {
			return this.dao;
		}

		//toutes les fonctions seront d�plac�es cot� serveur
		public void chargerFilsDeDicussions() throws Exception {	
			this.chargerGroupesUtilisateur();
			//ceux cree
			this.filDeDicussions = this.dao.listerFilsDeDiscussions(utilisateurCourant);
			
			//ceux des groupes
			for( Groupe g : this.groupes ) {
				this.dao.listerFilsDeDiscussions(g, this.filDeDicussions);
			}
			
			for(FilDeDiscussion f : this.filDeDicussions) {
				this.dao.listerMessagePourFilDeDiscussion(f);
			}

		}

		public void chargerGroupesUtilisateur() throws Exception {		
			this.groupes = this.dao.listerGroupesUtilisateur(this.utilisateurCourant);
		}
		
		public void chargerGroupesActifs() throws Exception {		
			
			for(FilDeDiscussion f : this.filDeDicussions){
				this.groupesActifs.add(this.dao.findGroupe(f.getIdGroupe()));
			}
		}

		public Set<FilDeDiscussion> getFilDeDicussions() {
			return this.filDeDicussions;
		}

		public Set<Groupe> getGroupesActifs() {
			return this.groupesActifs;
		}
		

		public Utilisateur getUtilisateurCourant() {
			return this.utilisateurCourant;
		}

		
		public void ajouterMessageEnAttente(Message m) {
			this.messageEnAttente.add(m);
		}
		
		
		public void chargerMapUtilisateurs() throws SQLException {
			this.mapUtilisateurs = this.dao.listerMapUtilisateurs();
		}
		
		private void chargerMapGroupes() throws SQLException {
			this.mapGroupes = this.dao.listerMapGroupes();
		}

		public Map<Integer, Utilisateur> getUtilisateursCourant() {
			return this.utilisateursCourant;
		}
		
		
		public void miseAJour() throws Exception {
			this.chargerMapUtilisateurs();
			this.chargerFilsDeDicussions();
			this.chargerGroupesActifs();
			this.chargerMapGroupes();
		}


		public ArrayList<Groupe> getGroupes() {
			return this.groupes;
		}
			
		
		public Map<String, Utilisateur> getMapUtilisateurs() {
			return mapUtilisateurs;
		}



		public void setMapUtilisateurs(Map<String, Utilisateur> mapUtilisateurs) {
			this.mapUtilisateurs = mapUtilisateurs;
		}


		public Map<String, Groupe> getMapGroupes() {
			return mapGroupes;
		}


		public void setMapGroupes(Map<String, Groupe> mapGroupes) {
			this.mapGroupes = mapGroupes;
		}


		public void chargerDonnees() {
			Donnees donnees = new Donnees();
			donnees.setFilDeDicussions(this.getFilDeDicussions());
			donnees.setGroupes(this.getGroupes());
			donnees.setGroupesActifs(groupesActifs);
			donnees.setMapUtilisateurs(mapUtilisateurs);
			donnees.setMapGroupes(mapGroupes);			
			
			this.serveur.envoyer(donnees);
		}
		
}
