package Serveur;

import java.awt.BorderLayout;
import java.awt.GridLayout;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

public class VueServeur extends JPanel {
	
	JTextArea  log;
	JScrollPane zoneLog;

	public VueServeur() throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		ControleurServeur controleur = new ControleurServeur(this);
		this.setLayout(new BorderLayout());
		
		//JPanel EAST
		JPanel est = new JPanel();
		GridLayout boutons = new GridLayout(1,1);
		est.setLayout(boutons);
		
			// JPanel pour les Boutons
			JPanel pButton = new JPanel();
			pButton.setLayout(new GridLayout(5, 1));


				JButton bAddUtil = new JButton();
				bAddUtil.setText("Ajouter un utilisateur");
				bAddUtil.addActionListener(controleur);
				pButton.add(bAddUtil);
				
				JButton bSuppUtil = new JButton();
				bSuppUtil.setText("Supprimer un utilisateur");
				bSuppUtil.addActionListener(controleur);
				pButton.add(bSuppUtil);
				
				JButton bAddGr = new JButton();
				bAddGr.setText("Ajouter un groupe");
				bAddGr.addActionListener(controleur);
				pButton.add(bAddGr);
				
				JButton bSuppGr = new JButton();
				bSuppGr.setText("Supprimer un groupe");
				bSuppGr.addActionListener(controleur);
				pButton.add(bSuppGr);
				
				JButton bAddGrToUtil = new JButton();
				bAddGrToUtil.setText("Allouer un groupe");
				bAddGrToUtil.addActionListener(controleur);
				pButton.add(bAddGrToUtil);
				
				
			est.add(pButton);
		this.add(est, BorderLayout.EAST);			
		
		log = new JTextArea();
		log.setAutoscrolls(true);
		log.setEditable(false);
		log.setLineWrap(true);
		log.setWrapStyleWord(true);
		log.append("**********************************************Bienvenue sur le serveur********************************************\n");
		zoneLog = new JScrollPane(log);	
		zoneLog.setAutoscrolls(true);
		this.add(zoneLog, BorderLayout.CENTER);
	}
	
	public void ecireLog(String message) {
		log.append(message + "\n");
	}
}
