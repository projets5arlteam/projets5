package Serveur;


import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.net.Socket;
import java.sql.SQLException;

import DAO.FilDeDiscussion;
import DAO.Message;
import DAO.Utilisateur;

public class Serveur extends Thread {
	public static final int PORT_NUMBER = 8081;

	//Socket server;
	protected Socket socket;
	ObjectInputStream oIn = null;
	ObjectOutputStream oOut = null;
	private ModeleServeur modele;

	public Serveur(Socket socket,ModeleServeur modele) {
		this.modele = modele;
		this.socket = socket;
		start();
	}

	public void run() {
		boolean connectionEffective = false;
		//System.out.println("Tentative de connexion en provenance de " + socket.getInetAddress().getHostAddress());

		oIn = null;
		oOut = null;
		Utilisateur util = new Utilisateur(null,null,null,null,null);
		try {
			oIn = new ObjectInputStream(socket.getInputStream()); 
			oOut = new ObjectOutputStream(socket.getOutputStream());
			while (!connectionEffective) {
				util = (Utilisateur) oIn.readObject(); 
				try {
					util = this.modele.getDao().findUtilisateur(util.getIdUtilisateur(), util.getMdp());
				} catch (SQLException e) {
					e.printStackTrace();
				}
				this.envoyer(util);
				if(util.getNom() != null) {
					connectionEffective = true;
					this.modele.setUtilisateurCourant(util);
					this.modele.miseAJour();
					this.modele.chargerDonnees();
				}
			}
			//on sort du while, on traite mtn
			boolean fin = false;
			String demande = (String) oIn.readObject();
			while(!fin) {
				switch (demande) {
					case "deconnexion" : 
						oIn.close();
						oOut.close();
						this.socket.close();
						fin = true;
						break;
					case "message" : 
						Message m = (Message) oIn.readObject();
						m = this.modele.getDao().createMessage(m);
						
						try {
							oOut.writeObject(m);
							oOut.flush();
						} catch (IOException e) {
							e.printStackTrace();
						}						
						break;
					case "filDeDiscussion":
						FilDeDiscussion fil = (FilDeDiscussion) oIn.readObject(); 
						fil = this.modele.getDao().createFilDeDiscussion(fil);
						this.modele.miseAJour();
						this.modele.chargerDonnees();
						break;
				}
				
				demande = (String) oIn.readObject();
			}

		} catch (IOException | ClassNotFoundException ex) {
			//System.out.println("Unable to get streams from client");
		}
		 catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	public void envoyer(Object o) {
		try {
			oOut.writeObject(o);
			oOut.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
}