package Serveur;


import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.net.ServerSocket;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

import com.mysql.jdbc.Util;

import DAO.ConnexionBD;
import DAO.DAO;
import DAO.Groupe;
import DAO.Utilisateur;
import net.miginfocom.swing.MigLayout;

public class ControleurServeur extends Thread implements ActionListener{
	
	public static final int PORT_NUMBER = 8081;
	
	public enum Etat {DEBUT,AJOUTERGROUPE,AJOUTERUTILISATEUR,ALLOUERGROUPE,SUPPUTIL,SUPPGROUPE}
	
	private VueServeur vue;
	private DAO dao;
	private Etat etatCourant;
	private JButton boutonCourant;
	
	
	//ajouter un groupe
	private JDialog fenetreAjouterGroupe;
	private JLabel label;
	private JLabel libelle;
	private JTextField texteLibelle;
	private JButton validerGroupe;
	
	//ajouter un utilisateur
	private JDialog fenetreAjouterUtilisateur;
	private JTextField texteNom;
	private JTextField textePrenom;
	private JTextField texteIdentifiant;
	private JTextField texteMdp;
	private JTextField texteType;
	private JComboBox comboBoxGroupes;	
	private JButton validerUtilisateur;
	
	//allouer un groupe
	private JDialog fenetreAllouerGroupe;
	private JComboBox comboBoxAllouerUtil;
	private JComboBox comboBoxAllouerGroupe;
	private JButton validerAllouerGroupe;
	
	//supprimer utilisateur
	private JDialog fenetreSuppUtil;
	private JComboBox comboBoxSuppUtil;
	private JButton validerSuppUtil;
	
	//supprimer groupe
	private JDialog fenetreSuppGroupe;
	private JComboBox comboBoxSuppGroupe;
	private JButton validerSuppGroupe;
	
	public ControleurServeur(VueServeur vue) throws ClassNotFoundException, InstantiationException, IllegalAccessException {
		this.vue = vue;
		this.dao = new DAO(ConnexionBD.getInstance());
		this.etatCourant = Etat.DEBUT;
		this.start();
		
	}

	@Override
	public void run() {
		ServerSocket server = null;
		try {
			server = new ServerSocket(PORT_NUMBER);
			while (true) {
				try {
					new ModeleServeur(server.accept());
				} catch (ClassNotFoundException | InstantiationException | IllegalAccessException e) {
					e.printStackTrace();
				}
			}
		} catch (IOException ex) {
			//System.out.println("Unable to start server.");
		} finally {
			try {
				if (server != null)
					server.close();
			} catch (IOException ex) {
				ex.printStackTrace();
			}
		}
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		//this.vue.ecireLog("Clic");

		switch(this.etatCourant) {
			
			case DEBUT : 
				this.boutonCourant = (JButton) arg0.getSource();
				switch (this.boutonCourant.getText()) {
					case "Ajouter un groupe" : 
						lancerAddGroupe();
						break;
					case "Ajouter un utilisateur" : 
							try {
								lancerAddUtilisateur();
							} catch (SQLException e1) {
								e1.printStackTrace();
							}
					  break;
					  
					case "Allouer un groupe" : 
						lancerAllouerGroupe();
						break;
					case "Supprimer un utilisateur" : 
						lancerSupprimerUtil();
						break;
					case "Supprimer un groupe" :
						lancerSupprimerGroupe();						
						break;
					  	
				}
				break;
				
			case AJOUTERGROUPE :
				this.boutonCourant = (JButton) arg0.getSource();
				this.etatCourant = Etat.DEBUT;
				if( this.boutonCourant.getText() == "Valider") {
					Groupe groupe = new Groupe(0,this.texteLibelle.getText());
					if( groupe.getLibelle().length() != 0) {
						try {
							this.dao.createGroupe(groupe);
						} catch (Exception e) {
							e.printStackTrace();
						}
						this.vue.ecireLog("Groupe ajout� � la BDD.");
					}
													
				}
				fenetreAjouterGroupe.dispose();		
				break;
								
			case AJOUTERUTILISATEUR :
				this.boutonCourant = (JButton) arg0.getSource();
				this.etatCourant = Etat.DEBUT;	
				if( this.boutonCourant.getText() == "Valider") {					
					Utilisateur utilAdd = new Utilisateur(this.texteIdentifiant.getText(),this.texteNom.getText(),this.textePrenom.getText(),this.texteMdp.getText(),this.texteType.getText());
					Groupe groupe = new Groupe(0,this.comboBoxGroupes.getSelectedItem().toString());
					try {
						groupe = this.dao.findGroupe(groupe.getLibelle());
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
					if(utilAdd.checkNull()) {
						try {
							this.dao.createUtilisateur(utilAdd,groupe);
							this.dao.createAppartenir(utilAdd, groupe);
							this.vue.ecireLog("Utilisateur ajout� � la BDD.");
						} catch (Exception e) {
							e.printStackTrace();
						}
					}
					else 
						this.vue.ecireLog("Erreur,Utilisateur non ajout� � la BDD.");
				}
				fenetreAjouterUtilisateur.dispose();
				break;
				
			case ALLOUERGROUPE :
				this.boutonCourant = (JButton) arg0.getSource();
				this.etatCourant = Etat.DEBUT;
				Utilisateur utilAllouer = new Utilisateur(null, null, null, null, null);;
				Groupe grAllouer = new Groupe(0, null);
				if( this.boutonCourant.getText() == "Valider") {
					utilAllouer.setIdUtilisateur(this.comboBoxAllouerUtil.getSelectedItem().toString());
					try {
						grAllouer = this.dao.findGroupe(this.comboBoxAllouerGroupe.getSelectedItem().toString());
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
					try {
						this.dao.createAppartenir(utilAllouer, grAllouer);
						this.vue.ecireLog("Ajout d'un utilisateur � un groupe effectu�.");
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				
				fenetreAllouerGroupe.dispose();
				break;
				
			case SUPPUTIL :
				this.boutonCourant = (JButton) arg0.getSource();
				this.etatCourant = Etat.DEBUT;
				Utilisateur utilSupp = new Utilisateur(null, null, null, null, null);
				if( this.boutonCourant.getText() == "Valider") {
					utilSupp.setIdUtilisateur(this.comboBoxSuppUtil.getSelectedItem().toString());
					try {
						this.dao.deleteUtilisateur(utilSupp);
						this.vue.ecireLog("Suppression de l'utilisateur de la BDD.");
					} catch (SQLException e) {
						e.printStackTrace();
					}
				}	
				fenetreSuppUtil.dispose();
				break;
				
			case SUPPGROUPE :
				this.boutonCourant = (JButton) arg0.getSource();
				this.etatCourant = Etat.DEBUT;
				Groupe groupeSupp = new Groupe(0, null);
				if( this.boutonCourant.getText() == "Valider") {
					groupeSupp.setLibelle(this.comboBoxSuppGroupe.getSelectedItem().toString());
					try {
						this.dao.deleteGroupe(groupeSupp);
						this.vue.ecireLog("Suppression du groupe de la BDD.");
					} catch (SQLException e) {
						e.printStackTrace();
					}				
				}	
				fenetreSuppGroupe.dispose();
				break;
		}
		
	}



	private void lancerAddGroupe() {
		this.etatCourant = Etat.AJOUTERGROUPE;
		fenetreAjouterGroupe = new JDialog();
		fenetreAjouterGroupe.getContentPane().setLayout(new MigLayout("", "[434px,grow]", "[][14px][][][][][][][][][][][][][][][][][][][][][][][]"));
		{
			JLabel lblAjouter = new JLabel("Cr�ation d'un groupe");
			fenetreAjouterGroupe.getContentPane().add(lblAjouter, "cell 0 0,alignx center,aligny top");
		}
		
		label = new JLabel("Libell� du groupe:");
		fenetreAjouterGroupe.getContentPane().add(label, "cell 0 3");
		
		texteLibelle = new JTextField();
		fenetreAjouterGroupe.getContentPane().add(texteLibelle, "cell 0 4,alignx left");
		texteLibelle.setColumns(10);
				
		validerGroupe = new JButton("Valider");
		validerGroupe.addActionListener(this);
		fenetreAjouterGroupe.getContentPane().add(validerGroupe, "cell 0 7,alignx center");
	
		
		fenetreAjouterGroupe.setSize(500, 180);
		fenetreAjouterGroupe.setMinimumSize(new Dimension(500, 180));
		fenetreAjouterGroupe.setTitle("Ajouter d'un Groupe");
		fenetreAjouterGroupe.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		fenetreAjouterGroupe.setVisible(true);
		
	}
	
	private void lancerAddUtilisateur() throws SQLException {
		this.etatCourant = Etat.AJOUTERUTILISATEUR;
		
		fenetreAjouterUtilisateur = new JDialog();
		
		fenetreAjouterUtilisateur.getContentPane().setLayout(new MigLayout("", "[434px,grow]", "[][14px][][][][][][][][][][][][][][][][][]"));
		{
			JLabel lblAjouter = new JLabel("Cr\u00E9ation d'un utilisateur");
			fenetreAjouterUtilisateur.getContentPane().add(lblAjouter, "cell 0 0,alignx center,aligny top");
		}
		
		JLabel label = new JLabel("Nom");
		fenetreAjouterUtilisateur.getContentPane().add(label, "cell 0 2");
		
		texteNom = new JTextField();
		fenetreAjouterUtilisateur.getContentPane().add(texteNom, "cell 0 3,alignx left");
		texteNom.setColumns(10);
		
		JLabel lblPrnom = new JLabel("Pr\u00E9nom");
		fenetreAjouterUtilisateur.getContentPane().add(lblPrnom, "cell 0 4");
		
		textePrenom = new JTextField();
		fenetreAjouterUtilisateur.getContentPane().add(textePrenom, "cell 0 5,alignx left");
		textePrenom.setColumns(10);
		
		JLabel lblIdentifiant = new JLabel("Identifiant");
		fenetreAjouterUtilisateur.getContentPane().add(lblIdentifiant, "cell 0 6");
		
		texteIdentifiant = new JTextField();
		fenetreAjouterUtilisateur.getContentPane().add(texteIdentifiant, "cell 0 7,alignx left");
		texteIdentifiant.setColumns(10);
		
		JLabel lblMotDePasse = new JLabel("Mot de passe");
		fenetreAjouterUtilisateur.getContentPane().add(lblMotDePasse, "cell 0 8");
		
		texteMdp = new JTextField();
		fenetreAjouterUtilisateur.getContentPane().add(texteMdp, "cell 0 9,alignx left");
		texteMdp.setColumns(10);
		
		JLabel lblType = new JLabel("Type d'utilisateur");
		fenetreAjouterUtilisateur.getContentPane().add(lblType, "cell 0 10,alignx left");
		
		texteType = new JTextField();
		fenetreAjouterUtilisateur.getContentPane().add(texteType, "cell 0 11,alignx left");
		texteType.setColumns(10);
		
		JLabel lblGroupe = new JLabel("Groupe");
		fenetreAjouterUtilisateur.getContentPane().add(lblGroupe, "cell 0 12");
		
	    comboBoxGroupes = new JComboBox();
		ArrayList<Groupe> listeGroupes = this.dao.listerGroupes();
		for (Groupe g : listeGroupes) {
			comboBoxGroupes.addItem(g.getLibelle());
		}
		
		fenetreAjouterUtilisateur.getContentPane().add(comboBoxGroupes, "cell 0 14,growx");

		validerUtilisateur = new JButton("Valider");
		validerUtilisateur.addActionListener(this);
		fenetreAjouterUtilisateur.getContentPane().add(validerUtilisateur, "cell 0 16,alignx center");
		fenetreAjouterUtilisateur.setSize(500, 400);
		fenetreAjouterUtilisateur.setMinimumSize(new Dimension(500, 400));
		fenetreAjouterUtilisateur.setTitle("Ajouter un Utilisateur");
		fenetreAjouterUtilisateur.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		fenetreAjouterUtilisateur.setVisible(true);
	}
	
	private void lancerAllouerGroupe() {
		
		this.etatCourant = Etat.ALLOUERGROUPE;
		
		fenetreAllouerGroupe = new JDialog();	
		
		fenetreAllouerGroupe.getContentPane().setLayout(new MigLayout("", "[434px,grow]", "[][][][][14px][][][][][][][][][][][][][][][][][][][][][][][][][]"));
		
		JLabel lblNewLabel = new JLabel("Ajout d'un groupe � un utilisateur");
		fenetreAllouerGroupe.getContentPane().add(lblNewLabel, "cell 0 0,alignx center");
		
		JLabel lblNewLabel_1 = new JLabel("Identifiant");
		fenetreAllouerGroupe.getContentPane().add(lblNewLabel_1, "cell 0 2");
		
		comboBoxAllouerUtil = new JComboBox();
		ArrayList<Utilisateur> listeUtilisateurs = null;
		try {
			listeUtilisateurs = this.dao.listerUtilisateurs();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		for (Utilisateur u : listeUtilisateurs) {
			comboBoxAllouerUtil.addItem(u.getIdUtilisateur());
		}
		fenetreAllouerGroupe.getContentPane().add(comboBoxAllouerUtil, "cell 0 3,growx");
		
		JLabel lblGroupe = new JLabel("Groupe");
		fenetreAllouerGroupe.getContentPane().add(lblGroupe, "cell 0 6");
		
		comboBoxAllouerGroupe = new JComboBox();
		ArrayList<Groupe> listeGroupes = null;
		try {
			listeGroupes = this.dao.listerGroupes();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		for (Groupe g : listeGroupes) {
			comboBoxAllouerGroupe.addItem(g.getLibelle());
		}
		fenetreAllouerGroupe.getContentPane().add(comboBoxAllouerGroupe, "cell 0 7,growx");
		
		validerAllouerGroupe = new JButton("Valider");
		fenetreAllouerGroupe.getContentPane().add(validerAllouerGroupe, "cell 0 12,alignx center");
		validerAllouerGroupe.addActionListener(this);
		fenetreAllouerGroupe.setSize(500, 250);
		fenetreAllouerGroupe.setMinimumSize(new Dimension(500, 250));
		fenetreAllouerGroupe.setTitle("Allouer un groupe");
		fenetreAllouerGroupe.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		fenetreAllouerGroupe.setVisible(true);
		
	}

	private void lancerSupprimerUtil() {
		this.etatCourant = Etat.SUPPUTIL;
		
		fenetreSuppUtil = new JDialog();
		
		fenetreSuppUtil.getContentPane().setLayout(new MigLayout("", "[434px,grow]", "[][14px][][][][][][][][][][][][][][][][][][][][][][]"));
		
		JLabel lblAjouter = new JLabel("Suppression d'un utilisateur");
		fenetreSuppUtil.getContentPane().add(lblAjouter, "cell 0 0,alignx center,aligny top");
		
		
		JLabel lblUtilisateur = new JLabel("Utilisateur");
		fenetreSuppUtil.getContentPane().add(lblUtilisateur, "cell 0 4");
		
		comboBoxSuppUtil = new JComboBox();
		ArrayList<Utilisateur> listeUtilisateurs = null;
		try {
			listeUtilisateurs = this.dao.listerUtilisateurs();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		for (Utilisateur u : listeUtilisateurs) {
			comboBoxSuppUtil.addItem(u.getIdUtilisateur());
		}
		fenetreSuppUtil.getContentPane().add(comboBoxSuppUtil, "cell 0 5,growx");
		
		validerSuppUtil = new JButton("Valider");
		fenetreSuppUtil.getContentPane().add(validerSuppUtil, "cell 0 7,alignx center");
		validerSuppUtil.addActionListener(this);
		fenetreSuppUtil.setSize(500, 180);
		fenetreSuppUtil.setMinimumSize(new Dimension(500, 180));
		fenetreSuppUtil.setTitle("Supprimer un utilisateur");
		fenetreSuppUtil.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		fenetreSuppUtil.setVisible(true);
	}
	
	private void lancerSupprimerGroupe() {
		this.etatCourant = Etat.SUPPGROUPE;
		
		fenetreSuppGroupe = new JDialog();
		
		fenetreSuppGroupe.getContentPane().setLayout(new MigLayout("", "[434px,grow]", "[][14px][][][][][][][][][][][][][][][][][][][][][][]"));
		
		JLabel lblAjouter = new JLabel("Suppression d'un groupe");
		fenetreSuppGroupe.getContentPane().add(lblAjouter, "cell 0 0,alignx center,aligny top");
		
		
		JLabel lblUtilisateur = new JLabel("Groupe");
		fenetreSuppGroupe.getContentPane().add(lblUtilisateur, "cell 0 4");
		
		comboBoxSuppGroupe = new JComboBox();
		ArrayList<Groupe> listeGroupes = null;
		try {
			listeGroupes = this.dao.listerGroupes();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		for (Groupe g : listeGroupes) {
			comboBoxSuppGroupe.addItem(g.getLibelle());
		}
		fenetreSuppGroupe.getContentPane().add(comboBoxSuppGroupe, "cell 0 5,growx");
		
		validerSuppGroupe = new JButton("Valider");
		fenetreSuppGroupe.getContentPane().add(validerSuppGroupe, "cell 0 7,alignx center");
		validerSuppGroupe.addActionListener(this);
		fenetreSuppGroupe.setSize(500, 180);
		fenetreSuppGroupe.setMinimumSize(new Dimension(500, 180));
		fenetreSuppGroupe.setTitle("Supprimer un utilisateur");
		fenetreSuppGroupe.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		fenetreSuppGroupe.setVisible(true);
	}
}
